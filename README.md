# RoDL-PoC

Proof of concept for automatic merging labels from different datasets, one-page summarization of datasets.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install rodl_poc.

```bash
pip install rodl_poc
```

## Usage

```python
import rodl_poc
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)


## Dev notes:

### Image annotation types:
- bounding boxes
- polygonal segmentation
- semantic segmentation
- instance segmentation
- 3D cuboids (depth information, volume, position in 3D space - measure the distance between objects)
- key-Point and Landmark (facial features, facial expressions, emotions, human body parts and poses)
- lines and splines (lane detection and recognition)

### VPN
- sudo apt-get install openfortivpn network-manager-fortisslvpn network-manager-fortisslvpn-gnome
- sudo openfortivpn vpn.edgelab.network:40443 -u siddhartha.kasaraneni@chalmersindustriteknik.se --trusted-cert 4253e0a6dcb5cc029e115ef365e60efdfc749a555311fdfe55e83135ef4431a1
- pass: Wy2kr362upC5XYc
- IP: 172.25.17.15

- ssh rodel@172.25.17.15
  usr: rodel
  pwd: rodel123

“clean” Ubuntu 20.04 LTS installation
Ram: 64GB
vCPU: 32
Storage: 200GB + 110GB for dataset
 
