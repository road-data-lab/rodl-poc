import pathlib, os, time, datetime
import numpy as np

time_stamp = datetime.datetime.now().isoformat()

#Folder names
data_root = 'data' #Main data folder
raw_data = 'raw'
interim = 'interim'
processed = 'processed'
train_test = 'train_test'


#Folder structure
project_dir = str(pathlib.Path(__file__).resolve().parents[0]) 
data_root_dir = os.path.join(project_dir, data_root)
interim_dir = os.path.join(project_dir,data_root, interim)
raw_data_dir = os.path.join(project_dir,data_root,raw_data)
processed_data_dir = os.path.join(project_dir,data_root, processed)
logs_dir = os.path.join(project_dir, "logs")
results_dir = os.path.join(project_dir, "results")
train_test_dir = os.path.join(data_root_dir,train_test)

# --> Files
patient_demographics_path = os.path.join(interim_dir,"demographic.csv")
file_info_path = os.path.join(interim_dir,"file_info.csv") 

expriments_storage = os.path.join(project_dir, "experiments.jsonl")

# --> Data processing variables
sampling_rate = 8000
interval = 500
breath_limit = 5000

# --> Interim folders
int_split = str(interval)+'ms_split'
breath_split = 'breath_split'
interval_split_dir = os.path.join(interim_dir, int_split)
breath_split_dir = os.path.join(interim_dir, breath_split)
interval_split_path = os.path.join(interim_dir, int_split+".csv")
breath_split_labels_path = os.path.join(interim_dir, "breath_split_labels.csv")

# --> Processed folders
image_uniform_split = 'img_' + int_split
processed_data_folder_path = os.path.join(processed_data_dir, image_uniform_split)
img_labels_path = os.path.join(processed_data_dir, image_uniform_split+'.csv')
img_breath_labels_name = 'img_' + breath_split
img_breath_labels_path = os.path.join(processed_data_dir, img_breath_labels_name+'.csv')

stockwell_uniform_split_name = 'St3_img_' + int_split
stockwell_uniform_split_path = os.path.join(processed_data_dir, stockwell_uniform_split_name)
stockwell_uniform_split_labels_path = os.path.join(processed_data_dir, stockwell_uniform_split_name+'.csv')


stockwell_nosplit_name = 'St_img_nosplit'
stockwell_nosplit_path = os.path.join(processed_data_dir, stockwell_nosplit_name)
stockwell_nosplit_labels_path = os.path.join(processed_data_dir, stockwell_nosplit_name+'.csv')

stockwell_breath_split_name = 'St_img_breath_split'
stockwell_breath_split_path = os.path.join(processed_data_dir, stockwell_breath_split_name)
stockwell_breath_split_labels_path = os.path.join(processed_data_dir, stockwell_breath_split_name+'.csv')


# --> Spectral features
NFFT = 150   # Window size for fft cannot be less then noverlap (128 by default)
fft_mode = 'psd' # other options are 'angle', 'phase'

# --> Model config
label_dict = {
    0:"normal",
    1:"crackle",
    2:"wheeze",
    3:"both"
}
label_dict_0 = {
    0:"normal",
    1:"crackle",
    2:"wheeze"
}

diagnosis_labels = {
    'Healthy':0,
    'URTI':1, 
    'COPD':2, 
    'Asthma':3 , 
    'LRTI':4, 
    'Bronchiectasis':5,
    'Pneumonia':6,
    'Bronchiolitis':7
}


learning_rate = 1e-4
batch_size=64
shuffle_size = 1000
epochs=10
steps_per_epoch=10
val_interval=5
val_steps=10
num_classes = len(label_dict)

image_size=224
input_shape=(image_size,image_size,3)

pretrained_model="mobilenet" # other options "inception", "resnet"
custom_model="DenseWrapper" # more models to be added here

experiment_name = "uniform_split_500ms" # modify this during runtime and pass as arguments
model_dir = os.path.join(project_dir, 'models')


# results
loss=100
accuracy=0
val_loss=100
val_accuracy=0
score=0
sensitivity=0
specificity=0
val_score=0
val_sensitivity=0
val_specificity=0
metrics={}
pred_score=0
pred_metrics={}


# Experiment config (needs to be updated on every run)
data_labels_path=""

# --> Logger config
log_format = "%(asctime)s,%(msecs)d %(levelname)-5s [%(filename)s:%(lineno)d] %(message)s"
logger_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": log_format
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "simple",
            "stream": "ext://sys.stdout"
        }
    },

    "root": {
        "level": "INFO",
        "handlers": ["console"]
    },
    "file_handler": True,
    "file_handler_config":{
        "maxBytes": 2e+7,
        "backupCount": 20
    }
}
