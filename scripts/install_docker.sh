# /bin/bash 

run_function() {
    echo "Excecuting function $1"
    if $1; then
        echo "Successfully excecuted! $1"
    else
        echo "Failed to excecuting!! $1"
    fi
}

install_default_packages() {
    apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install -qy \
        sudo \
        htop \
        openssh-server \
        git \
        ca-certificates \
        curl \
        screen \
        tmux \
        software-properties-common \
        python3 \
        python3-pip \
        python3-dev \
        libpq-dev \
        libsasl2-dev \
        libldap2-dev \
        libssl-dev \
        nano \
        iputils-ping \
        python3-venv \
        libatlas-base-dev 

    python3 -m pip install virtualenv

    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
    apt-get -qy upgrade
}

install_requirements(){
    apt-get update && apt-get install -qy \
        sshpass \
        ssh-keygen 
}

install_nvidia_driver() {
    apt remove --purge nvidia-*
    apt remove --purge libnvidia-*
    apt-get purge cuda
    apt-get autoremove
    apt update
    rm -rf /usr/local/cuda*


    echo >> ~/.bashrc '
    export PATH=/usr/local/cuda/bin:$PATH
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64'

}

install_docker() {
    curl -sSL https://get.docker.com | sh
    apt-get install -y docker-compose
    systemctl enable docker
    systemctl start docker
    usermod -aG docker ${USER}
    apt-get -qy upgrade
}

install_nvidia_docker() {
    curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
    distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
    curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
    sudo apt-get update
    apt-get install -y nvidia-docker2
    sudo systemctl restart docker
    sudo docker run --rm --gpus all nvidia/cuda:11.0-base nvidia-smi
}


run_function install_default_packages
# run_function install_docker
# run_function install_nvidia_docker