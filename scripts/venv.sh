#!/bin/bash

env_name="yolo_env";

echo $PWD

run_function() {
    echo "Excecuting function $1"
    if $1; then
        echo "Successfully excecuted! $1"
    else
        echo "Failed to excecuting!! $1"
    fi
}


check_venv() 
{
    virtualenv --version
}

activate() { 
    . ./$env_name/bin/activate
}

create_venv() {
    python3 -m venv $env_name
    . ./$env_name/bin/activate
    python3 -m pip install -r ./yolov5/requirements.txt
}

install_req() {
    apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        python3 \
        python3-pip \
        python3-dev \
        libpq-dev \
        libsasl2-dev \
        libldap2-dev \
        libssl-dev \
        nano \
        iputils-ping \
        python3-venv
        
    python3 -m pip install virtualenv
    
}

install_drivers() {
    apt install ubuntu-drivers-common
    
}


main() {
    if check_venv; then 
        if [ -d "$env_name" ]; then
            activate
            # echo "Activated vitual env"
        else
           run_function create_venv 
        fi
    else
        run_function install_req
        run_function create_venv
    fi
}


main