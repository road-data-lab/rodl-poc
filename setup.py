from setuptools import find_packages, setup
setup(
    name='rodl_poc',
    packages=find_packages(include=['rodl_poc']),
    version='0.1.0',
    description='Road Data Lab metadata proof of concept',
    author='Siddhartha Kasaraneni',
    license='MIT',
    install_requires=[],
    setup_requires=['pytest-runner'],
    tests_require=['pytest==4.4.1'],
    test_suite='tests',
)