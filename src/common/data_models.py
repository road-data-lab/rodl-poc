from attr import attrs, attrib
import attr, pytz
import typing
from typing import List
from datetime import date, datetime, time, timedelta
import cv2

@attr.s(auto_attribs=True) #, frozen=True)
class AnnotatedImage(object):
    image_path:str = attrib()
    annotation_path:str = attrib()
    dt: datetime = attrib()
    test: bool = attrib()