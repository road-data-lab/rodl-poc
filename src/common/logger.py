import os, sys, json, logging, logging.config, time, pathlib

from logging.handlers import RotatingFileHandler
from threading import Thread
from queue import Queue

project_folder_path = pathlib.Path(__file__).resolve().parents[2]
sys.path.append(str(project_folder_path))

import config
from src.common.utils import mkdir_p, AttrDict

class AsyncHandlerMixin(object):
    def __init__(self, *args, **kwargs):
        super(AsyncHandlerMixin, self).__init__(*args, **kwargs)
        self.__queue = Queue()
        self.__thread = Thread(target=self.__loop)
        self.__thread.daemon = True
        self.__thread.start()
        self.start_time = time.time()
        self.call_count = 0

    def emit(self, record):
        rate = round(self.call_count/(time.time()-self.start_time),2)
        if rate<5.0:
            self.__queue.put(record)
            self.call_count+=1
        
    def __loop(self):
        while True:
            time.sleep(0.1)
            record = self.__queue.get()
            try:
                super(AsyncHandlerMixin, self).emit(record)
            except:
                pass


class AsyncRotatingFileHandler(AsyncHandlerMixin, RotatingFileHandler):
    pass


def getLogger( logger_name,logfile = "common.log"):
    logger = logging.getLogger(logger_name)
    formatter = logging.Formatter(config.log_format)

    logging.config.dictConfig(config.logger_config)

    mkdir_p(config.logs_dir)
    
    logfilepath = os.path.join(config.logs_dir, logfile)

    if config.logger_config['file_handler']:
        file_handler = AsyncRotatingFileHandler(
            logfilepath,
            maxBytes=config.logger_config['file_handler_config']["maxBytes"],
            backupCount=config.logger_config['file_handler_config']["backupCount"]
        )
        
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    return logger

