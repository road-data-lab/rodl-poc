import os, sys, pathlib, errno, time, functools, json, uuid, subprocess
from types import ModuleType
import pandas as pd

project_dir = pathlib.Path(__file__).resolve().parents[2]

class AttrDict(dict):
  __getattr__ = dict.__getitem__
  __setattr__ = dict.__setitem__


def mkdir_p(path, folder_name = None):
    if folder_name:
        abs_folder_path = os.path.join(path, folder_name)
    else:
        abs_folder_path = path
    try:
        os.makedirs(abs_folder_path)
        
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(abs_folder_path):
            pass
        else:
            raise
    return abs_folder_path   

def get_size_mb(path = '.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return int(total_size / (1024 * 1024))

def find_all_files_in_folder(folder, extension):
    if os.path.exists(folder):
        paths= []
        check_paths = []
        for path, subdirs, files in os.walk(folder):
            for file_path in files:
                if file_path.endswith(extension) and not file_path.startswith('.'):
                    if not any(file_path in s for s in check_paths):
                        paths.append(os.path.join(path , file_path))
                        check_paths.append(file_path)
        return paths
    else:
        raise Exception("path does not exist -> "+ folder)
        
def find_files_in_folder(folder, extension):
    if os.path.exists(folder):
        paths= []
        for file in os.listdir(folder):
            if file.endswith(extension):
                paths.append(os.path.join(folder , file))

        return paths
    else:
        raise Exception("path does not exist -> "+ folder)

def get_uuid(size=8):
    return str(uuid.uuid4())[:size]

def dict_from_module(module):
    context = {}
    for setting in dir(module):
        if not setting.startswith("_") and not setting.startswith("logger"):
            v = getattr(module, setting)
            if not isinstance(v, ModuleType):
                val = getattr(module, setting)
                if setting.endswith("dir") or "path" in setting:
                    val = val.replace(str(project_dir), '*')
                context[setting] = val
    return context



def dump_jsonl(data, output_path, append=False) -> None:
    """
    Write list of objects to a JSON lines file.
    """
    try:
        mode = 'a+' if append else 'w'
        with open(output_path, mode, encoding='utf-8') as f:
            for line in data:
                json_record = json.dumps(line, ensure_ascii=False)
                f.write(json_record + '\n')
        # print('Wrote {} records to {}'.format(len(data), output_path))
    except:
        raise Exception(f"Cannot save to {output_path}")
        
def remove_unnamed(df:pd.DataFrame):
    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
    return df

def load_jsonl(input_path:str) -> list:
    """
    Read list of objects from a JSON lines file.
    """
    try:
        data = []
        with open(input_path, 'r', encoding='utf-8') as f:
            for line in f:
                data.append(json.loads(line.rstrip('\n|\r')))
        # print('Loaded {} records from {}'.format(len(data), input_path))
        return data
    except:
        raise Exception(f"Cannot load from {input_path}")

class Progress:
    def __init__(self,max_len, process_name):
        self.max_len = max_len
        self.process_name = process_name
        self.start_time = time.time()
        self.last_update_time = time.time()
        self.current_index = 0

    def update(self,n=1):
        self.current_index += n
        percent = int(self.current_index/self.max_len*100)
        current_time = time.time()
        iter_time = int(current_time - self.last_update_time)
        total_time = current_time - self.start_time
        self.last_update_time = current_time
        print(f"{self.process_name} :: {self.current_index} out of {self.max_len} complete {percent}% iter {iter_time}s -- total {round(total_time/60, 2)}min")


def launch_tensorboard(logdir, port=6006):
    if os.name == "posix":
        subprocess.Popen("sudo killall tensorboard", shell=True)
    command = "tensorboard --logdir="+ logdir + " --host 0.0.0.0 --port "+ str(port)
    subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    print("tensorboard launched at 0.0.0.0:"+ str(port))


def timer(func,logger=None):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = round((end_time - start_time),4)    # 3
        msg = "Finished " + str(func.__name__) + " in "+ str(run_time) + " secs"
        if logger:
            logger.info(msg)
        else:
            print(msg)
        return value
    return wrapper_timer

def try_except(func, *args, **kwargs):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try: 
            return func(*args, **kwargs)
        except:
            msg = "from>>"+str(func.__name__)
            if kwargs.__contains__('logger'):
                kwargs['logger'].exception(msg)
            else:
                print(msg)
            return False
    return wrapper