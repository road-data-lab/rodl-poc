import os,sys,pathlib
import folderstats

project_dir = pathlib.Path(__file__).resolve().parents[1]
sys.path.append(str(project_dir))

dataroot_dir = os.path.join(project_dir,'data')

df = folderstats.folderstats(dataroot_dir, ignore_hidden=True)
df.to_csv("folder_stats.csv")

print(df.head())

