import os, sys, json, pathlib, tqdm
import numpy as np
import pandas as pd
from datetime import date, datetime

project_folder_path = pathlib.Path(__file__).resolve().parents[1]
sys.path.append(str(project_folder_path))

import config
from src.common.utils import find_all_files_in_folder, mkdir_p
from src.common.data_models import AnnotatedImage

dataset_path = "/mnt/volvo-data/Volvo Highway Dataset v1.0/Data"


image_paths = find_all_files_in_folder(dataset_path,'.png')
annotation_paths = find_all_files_in_folder(dataset_path,'.json')
    
dataset = []

for img_path in tqdm.tqdm(image_paths):
    annotation_path = img_path.replace('.png', '.json').replace('imgs', 'annotations')

    dt = datetime.fromtimestamp(int(os.path.split(img_path)[1].split('.')[0].split('_')[-1])).isoformat() #,tz=pytz.timezone('Europe/Stockholm')

    annotated_image = AnnotatedImage(
        image_path=img_path,
        annotation_path=annotation_path,
        test=False if 'trainval' in img_path else True,
        dt=dt
    )
    
    dataset.append(annotated_image.__dict__)


file_info_df = pd.DataFrame(dataset)

mkdir_p(config.interim_dir)

file_info_df.to_csv( os.path.join(config.interim_dir,'volvo_dataset.csv'))